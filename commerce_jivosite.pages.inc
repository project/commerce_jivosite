<?php

/**
 *  @file
 *  Provide helper functions for create custom pages content.
 */

/**
 * Page callback for Jivosite's custom data update.
 */
function commerce_jivosite_ajax_set_custom_data() {
  $result = array();

  global $user;
  if ($user->uid) {
    $user_orders_link = commerce_jivosite_absolute_url("/user/{$user->uid}/orders");
    $result[] = array(
      'title'   => t('Customer'),
      'content' => t('Name: !name', array('!name' => $user->name)),
      'link' => $user_orders_link,
    );
  }
  else {
    $result[] = array(
      'title'   => t('Customer'),
      'content' => t('Not logged in'),
    );
  }

  $cart_order_id = commerce_cart_order_id($user->uid);
  if ($cart_order_id) {
    $cart_order_link = commerce_jivosite_absolute_url("/admin/commerce/orders/{$cart_order_id}/edit");
    $result[] = array(
      'title'   => t('Cart'),
      'content' => t('Order ID: !order_id', array('!order_id' => $cart_order_id)),
      'link' => $cart_order_link,
    );

    $order_total = _commerce_jivosite_load_order_total($cart_order_id);
    if ($order_total) {
      $order_total_string = commerce_currency_format($order_total['amount'], $order_total['currency_code']);
      $result[] = array(
        'content' => t('Order total: !order_total', array('!order_total' => $order_total_string)),
        'link' => $cart_order_link,
      );
    }
  }

  drupal_alter('commerce_jivosite_custom_data', $result);

	return array(
    '#type' => 'ajax',
    '#commands' => array(
      'command' => 'jivosite_data',
      'result'	=> $result,
    ),
  );
}

/**
 * @param string $url
 *
 * @return string
 */
function commerce_jivosite_absolute_url($url) {
  return url($url, array('attributes' => array('target'=>'_blank'), 'absolute' => TRUE));
}

/**
 * @param int $order_id
 *
 * @return array
 */
function _commerce_jivosite_load_order_total($order_id) {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'commerce_order')
    ->propertyCondition('order_id', $order_id);
  $result = $query->execute();

  if (!isset($result['commerce_order'])) {
    return array();
  }

  $orders = $result['commerce_order'];

  $fields = field_info_instances('commerce_order', 'commerce_order');
  $field_id = $fields['commerce_order_total']['field_id'];
  field_attach_load('commerce_order', $orders, FIELD_LOAD_CURRENT, array('field_id' => $field_id));
  $order = reset($orders);
  $value = field_get_items('commerce_order', $order, 'commerce_order_total');

  return !empty($value) ? $value[0] : array();
}
