/**
 * Ajax delivery command to send custom data to jivosite.
 */
(function($) {

Drupal.ajax.prototype.commands.jivosite_data = function (ajax, response, status) {
  if (response.result && typeof jivo_api !== 'undefined' && typeof jivo_api.setCustomData === 'function') {
    jivo_api.setCustomData(response.result);
  }
};

Drupal.behaviors.jivosite_data = {
  attach: function (context, settings) {
    // Updating user data on each page change, making sure chat is active.
    if (jivositeChatActive && typeof jivo_api !== 'undefined' && typeof jivo_api.setCustomData === 'function') {
      if (!jivositeDataCallWaiting) {
        jivositeDataCallWaiting = true;
        // We don't want too frequent updates so we "buffer" them and execute only one each 500ms.
        setTimeout(function () {
          jivositeAjaxGetData();
        }, 500);
      }
    }
  }
};

})(jQuery);

function jivositeAjaxGetData() {
  var prefBefore = Drupal.ajax.prototype.beforeSerialize;
  Drupal.ajax.prototype.beforeSerialize = function () {};

  new Drupal.ajax(null, '', {
    url: '/ajax/jivosite-set-data',
  }).eventResponse(false, {});

  jivositeDataCallWaiting = false;

  Drupal.ajax.prototype.beforeSerialize = prefBefore;
}

/**
 * @type {boolean} If Jivosite active and we need to send updates to Jivosites API.
 */
var jivositeChatActive = false;

/**
 * @type {boolean} If there has been data update recently that we need to send to jivosite (but we are waiting).
 */
var jivositeDataCallWaiting = false;

function jivo_onOpen() {
  jivositeChatActive = true;
  jivositeAjaxGetData();
}

function jivo_onClose(){
  jivositeChatActive = false;
}
